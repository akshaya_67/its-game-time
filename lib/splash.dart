import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:itsgametime/home.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override

  void initState(){
    super.initState();
    Timer(Duration(milliseconds: 1500), () =>
    {
      Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => HomePage(),
      )
      ) //pushReplacement
    }      
    );
  }

  Widget build(BuildContext context) {

    SystemChrome.setPreferredOrientations([ DeviceOrientation.portraitUp,]);
   
  return Scaffold(
      backgroundColor: Colors.indigo[500],
      body: Center(
        child: Text(
          'Its Game Time!', 
          style: TextStyle(
            fontSize: 50.0,
            color: Colors.white,
            ),
            maxLines: 3,
          ),
        ),
    );
  }
}