import 'dart:convert';
import 'dart:async';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:itsgametime/resultpage.dart';
import 'home.dart';

class Getjson extends StatelessWidget {

  String gamename;
  Getjson(this.gamename);
  String assettoload;

  // a function
  // sets the asset to a particular JSON file
  // and opens the JSON
  setasset() {
    if (gamename == "Word Play") {
      assettoload = "eng/eng.json";
    } else if (gamename == "G.K Quiz") {
     assettoload = "assets/quiz.json";
    } else
    {
      assettoload = "math/math.json";
    } 
  }

  @override
  Widget build(BuildContext context) {
    setasset();
    return FutureBuilder(
      future: DefaultAssetBundle.of(context).loadString(assettoload),
      builder: (context, snapshot){
        List mydata = json.decode(snapshot.data.toString());
        if(mydata == null){
          return Scaffold(
            body: Text('loading'),
          );
        }
        else{
          return PopUpScreen(mydata : mydata,gamename : gamename);
        }
      },      
    );
  }
}

class PopUpScreen extends StatelessWidget {
  int value;
  List mydata;
  String gamename;
  
  
  PopUpScreen({this.mydata,this.gamename});

 //Function to set variable timer for the different games

  @override
  Widget build(BuildContext context) {
    
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return Dialog(
      child: Container(
        height: 250,
        width: 350,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Center(
            child: Column(
            children: <Widget>[
             Text('Description for $gamename',
             style: TextStyle(
               fontSize: 20.0,
               fontWeight: FontWeight.w700,
               color: Colors.indigo,
             ),
             textAlign: TextAlign.center,),
             Text('1. Each question has a time limit of 10 sec.',
             textAlign: TextAlign.center,
             style: TextStyle(
               fontSize: 15.0,
               color: Colors.black,
             ),),
             Text('2. Enter the number of questions you want to attempt.',
             textAlign: TextAlign.center,
             style: TextStyle(
               fontSize: 15.0,
               color: Colors.black,
             ),),
             Text('(Max number of questions is 50)',
             textAlign: TextAlign.center,
             style: TextStyle(
               fontSize: 15.0,
               color: Colors.red,
             ),),
             TextField(            
            autofocus: true,
            keyboardType: TextInputType.number,
            onChanged: (number){
               value = int.parse(number);
            },
          ),
          ],
          ),
          ),
          RaisedButton(
            color: Colors.indigo,
            child: Text('Submit',
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w300
            )),
            onPressed:() {
              if((value == null)||(value == 0)||(value>50)){
              Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => CheckTextField(mydata: mydata,gamename : gamename)));
              }
              else{
                Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => QuizPage(mydata: mydata, value: value)));
              }
            }
            ),
        ],
        ),
    )
    );
  }
}

class CheckTextField extends StatelessWidget {

List mydata;
String gamename;
CheckTextField({this.mydata, this.gamename});

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    
      return Dialog(
        child: Container(
          height: 100,
          width: 100,
          child: Center(
          child: Column(
          children: <Widget>[
            Text('Alert!',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.red,
              fontSize: 20.0,
            )
            ),
            Text('Enter the number of questions!',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 15.0
            )),
            RaisedButton(
              color: Colors.indigo,
              child: Text('OK',
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w300,
              )),
              onPressed: () {
                Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => PopUpScreen(mydata: mydata,gamename : gamename)));
              },
              ),
        ],
        )
        )
        )
      );
       }
  }

class QuizPage extends StatefulWidget {

  List mydata;
  int value;


QuizPage({Key key, @required this.mydata, @required this.value}) : super(key : key);

  @override
  _QuizPageState createState() => _QuizPageState(mydata , value);
}

class _QuizPageState extends State<QuizPage> {
  
  List mydata;
  int value;
  

  _QuizPageState(this.mydata, this.value);

  Color colortoshow = Colors.indigo;
  Color right = Colors.green;
  Color wrong = Colors.red;
  int marks = 0;
  int j = 1;
  int i = 1;
  final _random = new Random();
  int timer = 10 ;
  String showtimer = "10" ;
  bool canceltimer = false;
  
   
  //random array

  var random_array;
  var distinctIds = [];
  var rand = new Random();

  void array_generation(){
  for (int k = 0; ;) {
    distinctIds.add(rand.nextInt(51));
    random_array = distinctIds.toSet().toList();
    if(random_array.length <= value){
          continue;
       }
       else{
           break;
         }
       }
  }

  Map<String , Color> btncolor = {
    "a" : Colors.indigo,
    "b" : Colors.indigo,
    "c" : Colors.indigo,
    "d" : Colors.indigo,
  };
  @override
  void initState(){
    starttimer();
    super.initState();
  }
  
  void starttimer() async {
    
    const onesec = Duration(seconds : 1);
    Timer.periodic(onesec, (Timer t) {
      setState(() {
        showtimer = timer.toString();
        if(timer < 1){
          t.cancel();
          nextquestion();
        }
        else if(canceltimer == true){
          t.cancel();
        }
        else{
          timer= timer - 1;
        }        
      });
     });
  }

  void nextquestion(){
    array_generation();
    canceltimer = false;
    timer = 10;
    //int seq = 3;
    
    setState(() {      
      if(j<value){
           i= random_array[j];
        j++;        
    }
    else{
      Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (context) => ResultPage(marks: marks, value: value)
        )
        );
    }

    btncolor['a'] = Colors.indigo;
    btncolor['b'] = Colors.indigo;
    btncolor['c'] = Colors.indigo;
    btncolor['d'] = Colors.indigo;

    starttimer();  
    }
    );
  }

  void checkAnswer(String k){
    if(mydata[2][i.toString()] == mydata[1][i.toString()][k]){
      marks = marks + 1;
      colortoshow = right;
    }
    else{
      colortoshow = wrong;
    }
    setState(() {
      btncolor[k] = colortoshow;
      canceltimer = true;
    });
    Timer((Duration(milliseconds : 500)), nextquestion);
  }

  Widget choicebutton (String k){
    return Padding(
      padding: EdgeInsets.symmetric(
        vertical : 10.0,
        horizontal: 20.0,
      ),
      child:MaterialButton(
        onPressed: () => checkAnswer(k),
        child: Text(mydata[1][i.toString()][k],
        style: TextStyle(
          color: Colors.white,
          fontSize: 15.0,
        )
        ),
        color: btncolor[k],
        splashColor: Colors.indigo[700],
        highlightColor: Colors.indigo[700],
        minWidth: 200.0,
        height: 40.0,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25.0),
        )
        ) 
    );
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
   
    return WillPopScope(
      onWillPop: (){
        return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text('Quiz'),
            content: Text('Do you want to Quit?'),
            actions: <Widget>[
              FlatButton(
                onPressed:(){
                   Navigator.of(context).pop();
                   }, 
                child: Text('NO'),
                ),
              FlatButton(
                onPressed:(){
                   Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => HomePage() ));
                   }, 
                child: Text('YES'),
                )
            ],
          )
        );          
      },
      child: Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Container(
              padding: EdgeInsets.all(15.0),
              alignment: Alignment.bottomCenter,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children : <Widget>[
                  Text('Q $j',
                  style: TextStyle(
                    fontSize: 30.0,
                    fontWeight: FontWeight.w500,
                    color: Colors.black,
                  )),
                  Text(mydata[0][i.toString()],
                  style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.w400,
                    color: Colors.indigo,
                  ),),
                  ],
                  ),
              )
          ),
          Expanded(
            flex: 6,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget> [
                 choicebutton('a'),
                 choicebutton('b'),
                 choicebutton('c'),
                 choicebutton('d'),
              ],
              )
           ),
          Expanded(
            flex: 1,
            child: Container(
              alignment: Alignment.topCenter,
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children : <Widget> [
                    Text('Time Left: ',
                style: TextStyle(
                  fontSize: 30.0,
                  color: Colors.red,
                  fontWeight: FontWeight.w400,
                ),
                ),
                    Text(showtimer,
                style: TextStyle(
                  fontSize: 30.0,
                  color: Colors.black,
                  fontWeight: FontWeight.w600,
                ),
                ),
                  ]
                ),
                )
              )
          ),
        ]
      )
    )
    );
  }
}