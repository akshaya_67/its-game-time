import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:itsgametime/splash.dart';
import 'quizpage.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  List<String> textdata = [
    "Play around with the world events",
    "Have fun with the English language",
    "Sharpen your brain with simple Math"
  ];

  Widget customcard(String gamename, String data){
     return Padding(
      padding: EdgeInsets.all(
        20.0
        ),
        child: InkWell(
          onTap: () {
            Navigator.of(context).pushReplacement(MaterialPageRoute(
              builder: (context) => Getjson(gamename), )
              );
          },
          child: Material(
            color: Colors.white,
            elevation: 10.0,
            child: Container(              
              height: MediaQuery.of(context).size.height / 6,
              width: MediaQuery.of(context).size.width,
              child: Column(                
                children: <Widget> [                   
                  Container(
                    alignment: Alignment.center,
                    child: Text('>>> $gamename' ,
                          style: TextStyle(
                             fontSize: 25.0,
                             fontWeight: FontWeight.w700,
                              color: Colors.indigo,
                            ),
                            textAlign: TextAlign.center,                                                  
                        ),
                        ),
                        Container(                          
                          child : Text(
                            data ,
                            style: TextStyle(
                              fontSize: 20.0,
                              color: Colors.deepPurple,
                              fontWeight: FontWeight.w600,                              
                            ),
                            textAlign: TextAlign.center,
                            )
                        )
                ]
              ) ,
              )
          )
        )
    );
  }
 
  @override  
  Widget build(BuildContext context) {
      SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp,]);   

    return WillPopScope(
      onWillPop: (){
        Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => SplashScreen() ));             
      },
      child:Scaffold(
      backgroundColor: Colors.indigo,
      body: Padding(
        padding: EdgeInsets.all(15.0),
      child: ListView(
        children: <Widget> [
          Text('Welcome to the Gaming Zone!',
              style: TextStyle(
                fontSize: 30.0,
                color: Colors.white,
              ),
              textAlign: TextAlign.center,
            ),
                  
          customcard("G.K Quiz", textdata[0]),
          customcard("Word Play" , textdata[1]),
          customcard("Quick Brain" , textdata[2]),
        ]
      ),
      ),
    )
    );
  }
}