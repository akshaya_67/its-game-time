import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'home.dart';

class ResultPage extends StatefulWidget {
  int marks;
  int value;

  ResultPage({Key key, @required this.marks , @required this.value}) : super(key : key);
  @override
  _ResultPageState createState() => _ResultPageState(marks, value);
}

class _ResultPageState extends State<ResultPage> {
  int marks;
  int total = 10;
  int value;
  String message;

  _ResultPageState(this.marks, this.value);

  void result_message(){
    if(marks == 0){
        message ='Sorry! Try Again.';
      }
    else if(marks<=value/3){
      message = 'Good!';
      }
      else if(marks<=(2/3)*value){
        message = 'Well Done! ';
      }
      else if(marks == value){
        message = 'Congratulations! Keep it up!';
      }      
      else{
        message = 'Excellent!';
      }
  }
   
  @override
  Widget build(BuildContext context) {

    result_message();
    
     SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
   
    return  WillPopScope(
      onWillPop: () {
        Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => HomePage() ));     
     },
      child: Scaffold(
      appBar: AppBar(
        title: Text(
          'Result',
          ),),
          body: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                flex: 4,
                child: Center(
                  child: Text(message,
                  style: TextStyle(
                    fontSize: 40.0,
                    color: Colors.indigo,
                    ),
                  ),
                 ),
                 ),
                  Expanded(
                    flex: 5,
                    child: Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget> [
                       Text('Your Score: ',
                     style: TextStyle(
                       fontSize: 40.0,
                     ),
                   ),
                      Text('$marks / $value',
                     style: TextStyle(
                       fontSize: 40.0,
                       color:  Colors.green,
                     ),
                   ),
                    ]
                  )
                )
              ),
              Expanded(
                flex: 6,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    OutlineButton(
                      onPressed: (){
                        Navigator.of(context).pushReplacement(MaterialPageRoute(
                          builder: (context) => HomePage() ));
                      },
                      child: Text('Continue',
                      style: TextStyle(
                        fontSize: 20.0,
                      ),
                      ),
                      padding: EdgeInsets.symmetric(
                        vertical: 10.0 ,
                        horizontal : 25.0 ,
                      ),
                      borderSide: BorderSide(width: 3.0, color: Colors.indigo),
                      splashColor: Colors.indigoAccent,
                    ),
                ],
                ),
              ),
            ],
            ),
    )
    );
  }
}