import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:itsgametime/splash.dart';

void main() {
   SystemChrome.setPreferredOrientations(
          [DeviceOrientation.portraitUp]);
       runApp(new MyApp());
   
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Its-Game-Time',
      theme: ThemeData(
        primarySwatch: Colors.indigo,
      ),
      home: SplashScreen(),      
    );
  }
}
